import telegram
import os
from flask import Flask, request, jsonify
import json
from queue import Queue
import threading
import time


def worker():
    while True:
        item = q.get()
        if item is None:
            break
        send_message(item)
        time.sleep(3)
        q.task_done()

q = Queue()
num_worker_threads = 1

threads = []
for i in range(num_worker_threads):
    t = threading.Thread(target=worker)
    t.start()
    threads.append(t)


def send_message(msg):
    api_token = os.getEnv('TELEGRAM_TOKEN')
    bot = telegram.Bot(token=api_token)
    bot.send_message(chat_id=-326957964, text=msg, parse_mode=telegram.ParseMode.MARKDOWN)

def send_depot_check_in_message(data):
    msg = """
*{}* met framenummer *{}* is binnengebracht bij het depot.
_Extra informatie:_ {}
_Ruwe data:_
```javascript
{}
```
""".format(data["bike"]["brand"]
        , data["bike"]["frame_number"], data["extra_information"], json.dumps(data, sort_keys=True, indent=4))
    q.put(msg)

def send_depot_check_out_message(data):
    msg = """
*{}* met framenummer *{}* is opgehaald bij het depot (of verwijderd). 
_Ruwe data:_ 
```javascript
{}
```
""".format(data["bike"]["brand"]
        , data["bike"]["frame_number"], json.dumps(data, sort_keys=True, indent=4))
    q.put(msg)

app = Flask(__name__)

@app.route('/event', methods=['POST'])
def handle_event():
    data = request.json
    if data["event_type"] == "check_in_depot":
        send_depot_check_in_message(data)
    elif data["event_type"] == "check_out_depot":
        send_depot_check_out_message(data)
    return "OK"

if __name__ == '__main__':
    app.run()
